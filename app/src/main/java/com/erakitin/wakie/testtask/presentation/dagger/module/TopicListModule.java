package com.erakitin.wakie.testtask.presentation.dagger.module;

import com.erakitin.wakie.testtask.domain.interactor.GetActiveTopicNotificationUseCase;
import com.erakitin.wakie.testtask.domain.interactor.GetNewTopicNotificationUseCase;
import com.erakitin.wakie.testtask.domain.interactor.GetTopicUpdateNotificationUseCase;
import com.erakitin.wakie.testtask.domain.interactor.GetTopicsUseCase;
import com.erakitin.wakie.testtask.presentation.presenter.ITopicListPresenter;
import com.erakitin.wakie.testtask.presentation.presenter.TopicListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by erakitin on 24/01/16.
 */
@Module
public class TopicListModule {

    @Provides
    public ITopicListPresenter provideTopicListPresenter(GetTopicsUseCase getTopicsUseCase,
                                                         GetNewTopicNotificationUseCase getNewTopicNotificationUseCase,
                                                         GetActiveTopicNotificationUseCase getActiveTopicNotificationUseCase,
                                                         GetTopicUpdateNotificationUseCase getTopicUpdateNotificationUseCase) {
        return new TopicListPresenter(getTopicsUseCase,
                getNewTopicNotificationUseCase,
                getActiveTopicNotificationUseCase,
                getTopicUpdateNotificationUseCase);
    }
}
