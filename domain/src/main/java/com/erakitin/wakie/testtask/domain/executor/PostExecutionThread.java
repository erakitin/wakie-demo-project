package com.erakitin.wakie.testtask.domain.executor;

import rx.Scheduler;

/**
 * Created by erakitin on 22/01/16.
 */
public interface PostExecutionThread {
    Scheduler getScheduler();
}
