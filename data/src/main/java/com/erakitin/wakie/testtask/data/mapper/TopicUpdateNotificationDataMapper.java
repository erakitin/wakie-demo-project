package com.erakitin.wakie.testtask.data.mapper;

import com.erakitin.wakie.testtask.data.model.ServerTopicUpdateNotification;
import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.domain.model.TopicUpdateNotification;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class TopicUpdateNotificationDataMapper extends BaseDataMapper<ServerTopicUpdateNotification, TopicUpdateNotification> {

    private final TopicDataMapper mTopicDataMapper;

    @Inject
    public TopicUpdateNotificationDataMapper(TopicDataMapper topicDataMapper) {
        mTopicDataMapper = topicDataMapper;
    }

    @Override
    protected TopicUpdateNotification transform(ServerTopicUpdateNotification serverEntity) {
        Topic topic = mTopicDataMapper.transform(serverEntity.getTopic());
        return new TopicUpdateNotification.Builder()
                .id(serverEntity.getId())
                .topic(topic)
                .build();
    }
}
