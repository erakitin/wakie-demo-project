package com.erakitin.wakie.testtask.data.datastore;

import com.erakitin.wakie.testtask.data.factories.TopicFactory;
import com.erakitin.wakie.testtask.data.model.ServerTopic;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class CloudTopicDataStore implements ITopicDataStore {

    private static final long LATENCY = 1000L;

    private final ServerTopicListStorage mServerTopicListStorage;
    private final TopicFactory mTopicFactory;

    @Inject
    public CloudTopicDataStore(ServerTopicListStorage serverTopicListStorage,
                               TopicFactory topicFactory) {
        mServerTopicListStorage = serverTopicListStorage;
        mTopicFactory = topicFactory;
    }

    @Override
    public Observable<List<ServerTopic>> getTopics(long lastId, int limit) {
        List<ServerTopic> topicList;
        if (lastId > 0) {
            topicList = mTopicFactory.buildTopicList(lastId, limit);
        } else {
            topicList = mTopicFactory.buildTopicList(limit);
        }
        mServerTopicListStorage.addTopics(topicList);
        return Observable.timer(LATENCY, TimeUnit.MILLISECONDS)
                .map(aLong -> topicList);
    }
}
