package com.erakitin.wakie.testtask.data.dagger;

import com.erakitin.wakie.testtask.data.datastore.CloudNotificationDataStore;
import com.erakitin.wakie.testtask.data.datastore.CloudTopicDataStore;
import com.erakitin.wakie.testtask.data.datastore.INotificationDataStore;
import com.erakitin.wakie.testtask.data.datastore.ITopicDataStore;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by erakitin on 22/01/16.
 */
@Module
public class DataStoreModule {

    @Singleton
    @Provides
    public INotificationDataStore provideNotificationDataStore(CloudNotificationDataStore cloudNotificationDataStore) {
        return cloudNotificationDataStore;
    }

    @Singleton
    @Provides
    public ITopicDataStore provideTopicDataStore(CloudTopicDataStore cloudTopicDataStore) {
        return cloudTopicDataStore;
    }
}
