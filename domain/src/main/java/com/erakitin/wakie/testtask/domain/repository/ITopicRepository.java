package com.erakitin.wakie.testtask.domain.repository;

import com.erakitin.wakie.testtask.domain.model.Topic;

import java.util.List;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public interface ITopicRepository {

    Observable<List<Topic>> getTopics(long lastId, int limit);
}
