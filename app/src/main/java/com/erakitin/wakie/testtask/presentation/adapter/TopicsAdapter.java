package com.erakitin.wakie.testtask.presentation.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.presentation.R;
import com.erakitin.wakie.testtask.presentation.foundation.FontSizeCalibrator;
import com.erakitin.wakie.testtask.presentation.foundation.Fonts;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by erakitin on 24/01/16.
 */
public class TopicsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_TOPIC_NORMAL = 1;
    private static final int TYPE_TOPIC_ACTIVE = 2;
    private static final int TYPE_LOADER       = 3;

    private static final int COMMENTS_COUNT_MAX = 99;

    private List<Topic> mTopics;
    private long mActiveTopicId = 52;
    private List<Long> mCheckedTopicIds = new ArrayList<>();

    private final LayoutInflater mInflater;
    private final Resources mResources;
    private final Context mContext;

    private boolean mHasMore;

    private final FontSizeCalibrator mContentFontSizeCalibrator;

    public TopicsAdapter(Activity activity) {
        mInflater = LayoutInflater.from(activity);
        mResources = activity.getResources();
        mContext = activity;

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float contentWidth = metrics.widthPixels -
                mResources.getDimension(R.dimen.topic_item_padding_horizontal) * 2 -
                mResources.getDimension(R.dimen.default_horizontal_margin) * 2;

        mContentFontSizeCalibrator = new FontSizeCalibrator(
                mResources.getDimension(R.dimen.text_topic_content_max_height),
                contentWidth,
                mResources.getDimension(R.dimen.text_topic_content_max),
                mResources.getDimension(R.dimen.text_topic_content_min),
                Fonts.PROXIMA_EXTRABOLD.getTypeface(mContext));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_LOADER:
                return new LoaderViewHolder(mInflater.inflate(R.layout.list_item_loader, parent, false));
            case TYPE_TOPIC_ACTIVE:
                return new ActiveTopicViewHolder(mInflater.inflate(R.layout.list_item_topic_active, parent, false));
            case TYPE_TOPIC_NORMAL:
                return new TopicViewHolder(mInflater.inflate(R.layout.list_item_topic, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BaseTopicViewHolder) {
            ((BaseTopicViewHolder)holder).bind(getItem(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mHasMore && position == getItemCount() - 1) {
            return TYPE_LOADER;
        } else if (getItem(position).getId() == mActiveTopicId) {
            return TYPE_TOPIC_ACTIVE;
        }
        return TYPE_TOPIC_NORMAL;
    }

    private Topic getItem(int position) {
        return mTopics.get(position);
    }

    @Override
    public int getItemCount() {
        if (mTopics == null) {
            return 0;
        }
        return mTopics.size() + (mHasMore ? 1 : 0);
    }

    public void setTopics(List<Topic> topics) {
        mTopics = topics;
        notifyDataSetChanged();
    }

    public void setActiveTopicId(long activeTopicId) {
        int currentActiveTopicPosition = findTopicPositionById(mActiveTopicId);
        int newActiveTopicPosition = findTopicPositionById(activeTopicId);

        mActiveTopicId = activeTopicId;

        if (currentActiveTopicPosition >= 0) {
            notifyItemChanged(currentActiveTopicPosition);
        }
        if (newActiveTopicPosition >= 0) {
            notifyItemChanged(newActiveTopicPosition);
        }
    }

    private int findTopicPositionById(long id) {
        for (int i = 0; i < mTopics.size(); i++) {
            if (mTopics.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public void setHasMore(boolean hasMore) {
        if (hasMore == mHasMore) {
            return;
        }
        mHasMore = hasMore;
        if (mHasMore) {
            notifyItemInserted(getItemCount() - 1);
        } else {
            notifyItemRemoved(getItemCount());
        }
    }

    private class LoaderViewHolder extends RecyclerView.ViewHolder {

        public LoaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    abstract class BaseTopicViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.sdvUserAvatar)
        SimpleDraweeView avatar;

        @Bind(R.id.tvUserName)
        TextView name;

        @Bind(R.id.tvUserStatus)
        TextView status;

        @Bind(R.id.tvTopicCreationTime)
        TextView createdAt;

        @Bind(R.id.tvTopicContent)
        TextView content;

        public BaseTopicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            name.setTypeface(Fonts.PROXIMA_BOLD.getTypeface(mContext));
            status.setTypeface(Fonts.PROXIMA_REGULAR.getTypeface(mContext));
            createdAt.setTypeface(Fonts.PROXIMA_REGULAR.getTypeface(mContext));
            content.setTypeface(Fonts.PROXIMA_EXTRABOLD.getTypeface(mContext));
        }

        public void bind(Topic topic) {
            name.setText(topic.getUser().getName());
            createdAt.setText(DateUtils.getRelativeTimeSpanString(topic.getCreationTime().getTime(),
                    System.currentTimeMillis(),
                    DateUtils.MINUTE_IN_MILLIS,
                    DateUtils.FORMAT_ABBREV_RELATIVE));

            String contentText = topic.getText();

            float fontSize = mContentFontSizeCalibrator.calibrateFontSize(contentText,
                    content.getLineSpacingExtra(),
                    content.getLineSpacingMultiplier());
            content.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);

            content.setText(contentText);

            setPlaceholder();
            if (topic.getUser().getAvatarUrl() != null) {
                avatar.setImageURI(Uri.parse(topic.getUser().getAvatarUrl()));
            }
        }

        private void setPlaceholder() {
            GenericDraweeHierarchy hierarchy = avatar.getHierarchy();
            hierarchy.setPlaceholderImage(R.drawable.placeholder_avatar);
            avatar.setHierarchy(hierarchy);
        }
    }

    class TopicViewHolder extends BaseTopicViewHolder {

        @Bind(R.id.btnCommentsCount)
        Button commentsCount;

        @Bind(R.id.tvReadyToDiscuss)
        CheckedTextView readyToDiscuss;

        public TopicViewHolder(View itemView) {
            super(itemView);
            commentsCount.setTypeface(Fonts.PROXIMA_BOLD.getTypeface(mContext));
            readyToDiscuss.setTypeface(Fonts.PROXIMA_REGULAR.getTypeface(mContext));
        }

        @Override
        public void bind(Topic topic) {
            super.bind(topic);
            if (topic.getCommentsCount() == 0) {
                commentsCount.setText(R.string.no_comment);
            } else if (topic.getCommentsCount() > COMMENTS_COUNT_MAX) {
                commentsCount.setText(R.string.too_much_comments);
            } else {
                commentsCount.setText(String.valueOf(topic.getCommentsCount()));
            }
            readyToDiscuss.setChecked(mCheckedTopicIds.contains(topic.getId()));
            readyToDiscuss.setOnClickListener(v -> {
                if (readyToDiscuss.isChecked()) {
                    mCheckedTopicIds.remove(topic.getId());
                } else {
                    mCheckedTopicIds.add(topic.getId());
                }
                readyToDiscuss.setChecked(!readyToDiscuss.isChecked());
            });
        }
    }

    class ActiveTopicViewHolder extends BaseTopicViewHolder {

        @Bind(R.id.tvCommentsCount)
        TextView commentsCount;

        @Bind(R.id.tvShareExperience)
        TextView shareExperience;

        public ActiveTopicViewHolder(View itemView) {
            super(itemView);
            commentsCount.setTypeface(Fonts.PROXIMA_REGULAR.getTypeface(mContext));
            shareExperience.setTypeface(Fonts.PROXIMA_REGULAR.getTypeface(mContext));
        }

        @Override
        public void bind(Topic topic) {
            super.bind(topic);
            commentsCount.setText(mResources.getQuantityString(R.plurals.number_of_comments, topic.getCommentsCount(), topic.getCommentsCount()));
        }
    }
}
