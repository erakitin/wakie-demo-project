package com.erakitin.wakie.testtask.presentation;

import android.app.Application;

import com.erakitin.wakie.testtask.presentation.dagger.component.AppComponent;
import com.erakitin.wakie.testtask.presentation.dagger.component.DaggerAppComponent;
import com.erakitin.wakie.testtask.presentation.dagger.module.AppModule;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by erakitin on 22/01/16.
 */
public class App extends Application {

    private AppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        setupGraph();
        Fresco.initialize(this);
    }

    private void setupGraph() {
        mComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        mComponent.inject(this);
    }

    public AppComponent getComponent() {
        return mComponent;
    }
}
