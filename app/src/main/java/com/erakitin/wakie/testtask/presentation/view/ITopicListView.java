package com.erakitin.wakie.testtask.presentation.view;

import com.erakitin.wakie.testtask.domain.model.Topic;

import java.util.List;

import rx.Observable;

/**
 * Created by erakitin on 24/01/16.
 */
public interface ITopicListView {
    void setTopics(List<Topic> topics, boolean hasMore);

    void notifyTopicsInserted(int start, int count, boolean hasMore);

    void notifyTopicInsertedToTop();

    void notifyTopicChanged(int position);

    void setActiveTopicId(long id);

    void showLoader();

    void hideLoader();

    Observable<Integer> getScrollObservable();
}
