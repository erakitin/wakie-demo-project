package com.erakitin.wakie.testtask.presentation.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.TextView;

import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.presentation.R;
import com.erakitin.wakie.testtask.presentation.adapter.TopicsAdapter;
import com.erakitin.wakie.testtask.presentation.dagger.component.AppComponent;
import com.erakitin.wakie.testtask.presentation.dagger.component.DaggerTopicListComponent;
import com.erakitin.wakie.testtask.presentation.dagger.module.TopicListModule;
import com.erakitin.wakie.testtask.presentation.foundation.Fonts;
import com.erakitin.wakie.testtask.presentation.foundation.RecyclerListScrollObservableUtils;
import com.erakitin.wakie.testtask.presentation.presenter.ITopicListPresenter;
import com.erakitin.wakie.testtask.presentation.view.ITopicListView;

import java.util.List;

import butterknife.Bind;
import rx.Observable;
import rx.Subscription;

public class TopicListActivity extends BasePresentedActivity<ITopicListPresenter> implements ITopicListView {

    @Bind(R.id.tvNotificationBadge)
    TextView mNotificationBadge;

    @Bind(R.id.tvUserBadge)
    TextView mUserBadge;

    @Bind(R.id.rvList)
    RecyclerView mList;

    @Bind(R.id.etSearch)
    EditText mSearchView;

    @Bind(R.id.vSearchPanelShadow)
    View mSearchPanelShadowView;

    @Bind(R.id.pbLoader)
    View mLoader;

    private TopicsAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    private AccelerateInterpolator mShadowShowingInterpolator = new AccelerateInterpolator();
    private Subscription mScrollSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_list);

        setUpViews();
        mPresenter.init(this);
    }

    private void setUpViews() {
        mSearchView.setTypeface(Fonts.PROXIMA_REGULAR.getTypeface(this));

        mAdapter = new TopicsAdapter(this);
        mList.setAdapter(mAdapter);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mList.setLayoutManager(mLinearLayoutManager);

        subscribeToFirstItemOffsetUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
        unsubscribeFromOffsetUpdates();
    }

    @Override
    public void setTopics(List<Topic> topics, boolean hasMore) {
        mAdapter.setTopics(topics);
        mAdapter.setHasMore(hasMore);
    }

    @Override
    public void notifyTopicsInserted(int start, int count, boolean hasMore) {
        mAdapter.notifyItemRangeInserted(start, count);
        mAdapter.setHasMore(hasMore);
    }

    @Override
    public void notifyTopicInsertedToTop() {
        mAdapter.notifyItemInserted(0);
    }

    @Override
    public void notifyTopicChanged(int position) {
        mAdapter.notifyItemChanged(position);
    }

    @Override
    public void setActiveTopicId(long id) {
        mAdapter.setActiveTopicId(id);
    }

    @Override
    public void showLoader() {
        mLoader.setVisibility(View.VISIBLE);
        mList.setVisibility(View.GONE);
    }

    @Override
    public void hideLoader() {
        mLoader.setVisibility(View.GONE);
        mList.setVisibility(View.VISIBLE);
    }

    @Override
    public Observable<Integer> getScrollObservable() {
        return RecyclerListScrollObservableUtils
                .lastPositionUpdatesObservable(mList, mLinearLayoutManager);
    }

    private void unsubscribeFromOffsetUpdates() {
        if (mScrollSubscription != null) {
            mScrollSubscription.unsubscribe();
        }
    }

    private void subscribeToFirstItemOffsetUpdates() {
        unsubscribeFromOffsetUpdates();

        int maxOffset = getResources().getDimensionPixelSize(R.dimen.max_offset_to_show_shadow);
        mScrollSubscription = RecyclerListScrollObservableUtils
                .firstItemOffsetUpdatesObservable(mList, mLinearLayoutManager, maxOffset)
                .distinctUntilChanged()
                .map(offset -> calculateShadowAlpha(offset, maxOffset))
                .subscribe(alpha -> mSearchPanelShadowView.setAlpha(alpha));
    }

    private float calculateShadowAlpha(int offset, float maxOffset) {
        return mShadowShowingInterpolator.getInterpolation(((float)offset) / maxOffset);
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerTopicListComponent.builder()
                .appComponent(appComponent)
                .topicListModule(new TopicListModule())
                .build()
                .inject(this);
    }
}
