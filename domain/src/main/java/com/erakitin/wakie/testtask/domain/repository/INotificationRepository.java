package com.erakitin.wakie.testtask.domain.repository;

import com.erakitin.wakie.testtask.domain.model.ActiveTopicNotification;
import com.erakitin.wakie.testtask.domain.model.NewTopicNotification;
import com.erakitin.wakie.testtask.domain.model.TopicUpdateNotification;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public interface INotificationRepository {

    Observable<ActiveTopicNotification> getActiveTopicNotification();

    Observable<NewTopicNotification> getNewTopicNotification();

    Observable<TopicUpdateNotification> getTopicUpdateNotification();
}
