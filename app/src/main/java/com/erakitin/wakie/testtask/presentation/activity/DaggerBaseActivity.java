package com.erakitin.wakie.testtask.presentation.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.erakitin.wakie.testtask.presentation.App;
import com.erakitin.wakie.testtask.presentation.dagger.component.AppComponent;

/**
 * Created by erakitin on 22/01/16.
 */
public abstract class DaggerBaseActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setupComponent(((App)getApplication()).getComponent());
        super.onCreate(savedInstanceState);
    }

    protected abstract void setupComponent(AppComponent appComponent);
}
