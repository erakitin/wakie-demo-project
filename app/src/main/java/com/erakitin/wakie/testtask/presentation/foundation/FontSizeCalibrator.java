package com.erakitin.wakie.testtask.presentation.foundation;

import android.graphics.Typeface;
import android.text.TextPaint;

/**
 * Created by erakitin on 25/01/16.
 */
public class FontSizeCalibrator {

    private static final float TEXT_SIZE_ACCURACY = .2f;

    private final float mMaxTextHeight;
    private final float mMaxTextWidth;
    private final float mMaxFontSize;
    private final float mMinFontSize;

    private final TextPaint mPaint;
    private CharSequence mText;

    private float mLineSpacingMultiplier;
    private float mLineSpacingExtra;

    public FontSizeCalibrator(float maxTextHeight,
                              float maxTextWidth,
                              float maxFontSize,
                              float minFontSize,
                              Typeface typeface) {
        mMaxTextHeight = maxTextHeight;
        mMaxTextWidth = maxTextWidth;
        mMaxFontSize = maxFontSize;
        mMinFontSize = minFontSize;

        mPaint = new TextPaint();
        mPaint.setTypeface(typeface);
    }

    public float calibrateFontSize(CharSequence text, float lineSpacingExtra, float lineSpacingMultiplier) {
        mLineSpacingMultiplier = lineSpacingMultiplier;
        mLineSpacingExtra = lineSpacingExtra;
        mText = text;

        mPaint.setTextSize(mMaxFontSize);
        float height = calculateTextHeight();
        if (Float.compare(mMaxTextHeight, height) >= 0) {
            return mMaxFontSize;
        }

        mPaint.setTextSize(mMinFontSize);
        height = calculateTextHeight();
        if (Float.compare(height, mMaxTextHeight) >= 0) {
            return mMinFontSize;
        }

        return calculateFontSize(mMaxFontSize, mMinFontSize);
    }

    private float calculateFontSize(float maxSize, float minSize) {
        if (Float.compare(maxSize - minSize, TEXT_SIZE_ACCURACY) <= 0) {
            return minSize;
        }

        float newSize = (maxSize + minSize) / 2;
        mPaint.setTextSize(newSize);
        float height = calculateTextHeight();

        if (Float.compare(height, mMaxTextHeight) > 0) {
            return calculateFontSize(newSize, minSize);
        } else {
            return calculateFontSize(maxSize, newSize);
        }
    }

    private float calculateTextHeight() {
        int lineCount = getLineCount(mMaxTextWidth, mText);
        return lineCount * getLineHeight() + (lineCount > 0 ? (lineCount - 1) * mPaint.getFontSpacing() : 0);
    }

    private int getLineCount(float maxWidth, CharSequence text) {
        int lineCount = 0;
        int index = 0;
        while (index < text.length()) {
            index += mPaint.breakText(text, index, text.length(), true, maxWidth, null);
            lineCount++;
        }
        return lineCount;
    }

    private float getLineHeight() {
        return mPaint.getFontMetricsInt(null) * mLineSpacingMultiplier + mLineSpacingExtra;
    }
}
