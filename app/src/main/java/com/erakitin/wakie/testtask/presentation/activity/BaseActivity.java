package com.erakitin.wakie.testtask.presentation.activity;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by erakitin on 22/01/16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        bindViews();
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }
}
