package com.erakitin.wakie.testtask.data.datastore;

import com.erakitin.wakie.testtask.data.model.ServerTopic;

import java.util.List;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public interface ITopicDataStore {

    Observable<List<ServerTopic>> getTopics(long lastId, int limit);
}
