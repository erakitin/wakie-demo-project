package com.erakitin.wakie.testtask.presentation.dagger.component;

import com.erakitin.wakie.testtask.presentation.activity.TopicListActivity;
import com.erakitin.wakie.testtask.presentation.dagger.module.TopicListModule;
import com.erakitin.wakie.testtask.presentation.dagger.scope.ActivityScope;

import dagger.Component;

/**
 * Created by erakitin on 24/01/16.
 */
@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = TopicListModule.class
)
public interface TopicListComponent {

    void inject(TopicListActivity activity);
}
