package com.erakitin.wakie.testtask.domain.model;

/**
 * Created by erakitin on 21/01/16.
 */
public interface EntityBuilder<T> {
    T build();
}
