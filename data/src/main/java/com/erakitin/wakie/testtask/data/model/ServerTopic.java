package com.erakitin.wakie.testtask.data.model;

/**
 * Created by erakitin on 20/01/16.
 */
public class ServerTopic extends ServerEntity {

    private ServerUser mUser;
    private String mText;
    private long mTimestamp;
    private int mCommentsCount;

    public ServerUser getUser() {
        return mUser;
    }

    public void setUser(ServerUser user) {
        mUser = user;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(long timestamp) {
        mTimestamp = timestamp;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        mCommentsCount = commentsCount;
    }
}
