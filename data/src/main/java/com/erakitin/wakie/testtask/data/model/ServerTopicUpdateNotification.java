package com.erakitin.wakie.testtask.data.model;

/**
 * Created by erakitin on 21/01/16.
 */
public class ServerTopicUpdateNotification extends ServerEntity {

    private ServerTopic mTopic;

    public ServerTopic getTopic() {
        return mTopic;
    }

    public void setTopic(ServerTopic topic) {
        mTopic = topic;
    }
}
