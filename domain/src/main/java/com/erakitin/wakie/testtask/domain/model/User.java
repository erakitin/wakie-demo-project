package com.erakitin.wakie.testtask.domain.model;

/**
 * Created by erakitin on 21/01/16.
 */
public class User extends Entity {

    private final String mName;
    private final String mAvatarUrl;

    public String getName() {
        return mName;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    private User(Builder builder) {
        super(builder.mId);
        mName = builder.mName;
        mAvatarUrl = builder.mAvatarUrl;
    }

    public static class Builder implements EntityBuilder<User> {
        private long mId;
        private String mName;
        private String mAvatarUrl;

        public Builder() {
        }

        public Builder id(long id) {
            this.mId = id;
            return this;
        }

        public Builder name(String name) {
            mName = name;
            return this;
        }

        public Builder avatarUrl(String avatarUrl) {
            mAvatarUrl = avatarUrl;
            return this;
        }

        @Override
        public User build() {
            return new User(this);
        }
    }
}
