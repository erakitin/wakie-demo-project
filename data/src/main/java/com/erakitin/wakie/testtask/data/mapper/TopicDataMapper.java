package com.erakitin.wakie.testtask.data.mapper;

import com.erakitin.wakie.testtask.data.model.ServerTopic;
import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.domain.model.User;

import java.util.Date;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class TopicDataMapper extends BaseDataMapper<ServerTopic, Topic> {

    private final UserDataMapper mUserDataMapper;

    @Inject
    public TopicDataMapper(UserDataMapper userDataMapper) {
        mUserDataMapper = userDataMapper;
    }

    @Override
    protected Topic transform(ServerTopic serverEntity) {
        User user = mUserDataMapper.transform(serverEntity.getUser());
        return new Topic.Builder()
                .id(serverEntity.getId())
                .user(user)
                .text(serverEntity.getText())
                .creationTime(new Date(serverEntity.getTimestamp()))
                .commentsCount(serverEntity.getCommentsCount())
                .build();
    }
}
