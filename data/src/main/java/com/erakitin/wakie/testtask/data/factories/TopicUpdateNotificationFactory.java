package com.erakitin.wakie.testtask.data.factories;

import com.erakitin.wakie.testtask.data.model.ServerTopic;
import com.erakitin.wakie.testtask.data.model.ServerTopicUpdateNotification;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class TopicUpdateNotificationFactory {

    private long mLastId = 0;

    private final Random mRandom = new Random();

    private final TopicFactory mTopicFactory;

    @Inject
    public TopicUpdateNotificationFactory(TopicFactory topicFactory) {
        mTopicFactory = topicFactory;
    }

    public ServerTopicUpdateNotification buildNotificaton(List<ServerTopic> topicList) {
        ServerTopic randomTopic = topicList.get(mRandom.nextInt(topicList.size()));
        mTopicFactory.modifyTopic(randomTopic);

        ServerTopicUpdateNotification notification = new ServerTopicUpdateNotification();
        notification.setId(++mLastId);
        notification.setTopic(randomTopic);

        return notification;
    }
}
