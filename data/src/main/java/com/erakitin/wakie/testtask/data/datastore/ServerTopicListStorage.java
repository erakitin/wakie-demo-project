package com.erakitin.wakie.testtask.data.datastore;

import com.erakitin.wakie.testtask.data.model.ServerTopic;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by erakitin on 22/01/16.
 */
@Singleton
public class ServerTopicListStorage {

    @Inject
    public ServerTopicListStorage() {
    }

    private List<ServerTopic> mTopicList = new ArrayList<>();

    public List<ServerTopic> getTopicList() {
        return mTopicList;
    }

    public void addTopic(ServerTopic topic) {
        mTopicList.add(topic);
    }

    public void addTopics(List<ServerTopic> topicList) {
        mTopicList.addAll(topicList);
    }
}
