package com.erakitin.wakie.testtask.presentation.foundation;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Created by erakitin on 24/01/16.
 */
public enum Fonts {

    PROXIMA_SEMIBOLD("Proxima Nova Bold.otf"),
    PROXIMA_REGULAR("Proxima Nova Regular.otf"),
    PROXIMA_EXTRABOLD("Proxima Nova Extrabold.otf"),
    PROXIMA_BOLD("Proxima Nova Bold.otf");

    private static final String TAG = Fonts.class.getName();

    private static final Hashtable<String, Typeface> sCache = new Hashtable<>();

    private String mFilename;

    Fonts(String filename) {
        mFilename = filename;
    }

    public Typeface getTypeface(Context context) {
        synchronized (sCache) {
            if (!sCache.containsKey(mFilename)) {
                try {
                    Typeface typeface = Typeface.createFromAsset(context.getApplicationContext().getAssets(), "fonts/" + mFilename);
                    sCache.put(mFilename, typeface);
                } catch (Exception e) {
                    Log.e(TAG, "Could not get typeface '" + mFilename
                            + "' because " + e.getMessage(), e);
                    return null;
                }
            }
            return sCache.get(mFilename);
        }
    }
}
