package com.erakitin.wakie.testtask.domain.model;

/**
 * Created by erakitin on 21/01/16.
 */
public class NewTopicNotification extends Entity {

    private Topic mTopic;

    public Topic getTopic() {
        return mTopic;
    }

    private NewTopicNotification(Builder builder) {
        super(builder.mId);
        mTopic = builder.mTopic;
    }

    public static class Builder implements EntityBuilder<NewTopicNotification> {

        private long mId;
        private Topic mTopic;

        public Builder id(long id) {
            mId = id;
            return this;
        }

        public Builder topic(Topic topic) {
            mTopic = topic;
            return this;
        }

        @Override
        public NewTopicNotification build() {
            return new NewTopicNotification(this);
        }
    }
}
