package com.erakitin.wakie.testtask.data.model;

/**
 * Created by erakitin on 20/01/16.
 */
public abstract class ServerEntity {

    private long mId;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }
}
