package com.erakitin.wakie.testtask.data.model;

/**
 * Created by erakitin on 20/01/16.
 */
public class ServerUser extends ServerEntity {

    private String mName;
    private String mAvatarUrl;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        mAvatarUrl = avatarUrl;
    }
}
