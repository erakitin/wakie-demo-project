package com.erakitin.wakie.testtask.presentation.activity;

import javax.inject.Inject;

/**
 * Created by erakitin on 24/01/16.
 */
public abstract class BasePresentedActivity<PRESENTER> extends DaggerBaseActivity {

    @Inject
    PRESENTER mPresenter;

}
