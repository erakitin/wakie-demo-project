package com.erakitin.wakie.testtask.presentation.foundation;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import rx.Observable;
import rx.subscriptions.Subscriptions;

/**
 * Created by erakitin on 25/01/16.
 */
public final class RecyclerListScrollObservableUtils {

    private static final int POSITION_FROM_BOTTOM_TO_UPDATE = 3;

    public static Observable<Integer> lastPositionUpdatesObservable(final RecyclerView rv, final LinearLayoutManager layoutManager) {
        return Observable.create(subscriber -> {
            final RecyclerView.OnScrollListener sl = new RecyclerView.OnScrollListener() {

                private int mCurrentPosition;
                private int mUpdatePosition;

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (!subscriber.isUnsubscribed()) {
                        mCurrentPosition = layoutManager.findLastVisibleItemPosition();
                        mUpdatePosition = rv.getAdapter().getItemCount() - 1 - POSITION_FROM_BOTTOM_TO_UPDATE;
                        if (mCurrentPosition >= mUpdatePosition) {
                            subscriber.onNext(rv.getAdapter().getItemCount());
                        }
                    }
                }
            };
            rv.addOnScrollListener(sl);
            subscriber.add(Subscriptions.create(() -> rv.removeOnScrollListener(sl)));
        });
    }

    public static Observable<Integer> firstItemOffsetUpdatesObservable(final RecyclerView rv, final LinearLayoutManager layoutManager, int maxOffset) {
        return Observable.create(subscriber -> {
            final RecyclerView.OnScrollListener sl = new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (subscriber.isUnsubscribed()) {
                        return;
                    }
                    int offset;
                    if (recyclerView.getAdapter().getItemCount() == 0) {
                        offset = 0;
                    } else if (layoutManager.findFirstVisibleItemPosition() > 0) {
                        offset = maxOffset;
                    } else {
                        offset = rv.getPaddingTop() - rv.getChildAt(0).getTop();
                    }

                    subscriber.onNext(Math.min(offset, maxOffset));
                }
            };
            rv.addOnScrollListener(sl);
            subscriber.add(Subscriptions.create(() -> rv.removeOnScrollListener(sl)));
        });
    }
}
