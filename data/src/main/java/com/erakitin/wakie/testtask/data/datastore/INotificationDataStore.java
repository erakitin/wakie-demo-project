package com.erakitin.wakie.testtask.data.datastore;


import com.erakitin.wakie.testtask.data.model.ServerActiveTopicNotification;
import com.erakitin.wakie.testtask.data.model.ServerNewTopicNotification;
import com.erakitin.wakie.testtask.data.model.ServerTopicUpdateNotification;

import rx.Observable;


/**
 * Created by erakitin on 22/01/16.
 */
public interface INotificationDataStore {

    Observable<ServerActiveTopicNotification> getActiveTopicNotification();

    Observable<ServerNewTopicNotification> getNewTopicNotification();

    Observable<ServerTopicUpdateNotification> getTopicUpdateNotification();
}
