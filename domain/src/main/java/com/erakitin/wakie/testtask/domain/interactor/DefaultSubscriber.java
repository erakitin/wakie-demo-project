package com.erakitin.wakie.testtask.domain.interactor;

import android.util.Log;

import rx.Subscriber;

/**
 * Created by erakitin on 24/01/16.
 */
public abstract class DefaultSubscriber<T> extends rx.Subscriber<T> {

    private static final String TAG = "DefaultSubscriber";

    private final Exception mSubscribeStackTrace;

    public DefaultSubscriber() {
        mSubscribeStackTrace = new Exception();
    }

    public DefaultSubscriber(Subscriber<?> subscriber) {
        super(subscriber);
        mSubscribeStackTrace = new Exception();
    }

    public DefaultSubscriber(Subscriber<?> subscriber, boolean shareSubscriptions) {
        super(subscriber, shareSubscriptions);
        mSubscribeStackTrace = new Exception();
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable ex) {
        Log.w(TAG, "onError cause " + ex.getMessage(), ex);
        Log.w(TAG, "onError stack trace ", mSubscribeStackTrace);
    }

    @Override
    public void onNext(T t) {

    }
}