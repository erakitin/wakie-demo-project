package com.erakitin.wakie.testtask.data.mapper;

import com.erakitin.wakie.testtask.data.model.ServerUser;
import com.erakitin.wakie.testtask.domain.model.User;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class UserDataMapper extends BaseDataMapper<ServerUser, User> {

    @Inject
    public UserDataMapper() {
    }

    @Override
    protected User transform(ServerUser serverEntity) {
        return new User.Builder()
                .id(serverEntity.getId())
                .name(serverEntity.getName())
                .avatarUrl(serverEntity.getAvatarUrl())
                .build();
    }
}
