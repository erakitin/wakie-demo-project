package com.erakitin.wakie.testtask.presentation.dagger.module;

import android.app.Application;

import com.erakitin.wakie.testtask.domain.executor.PostExecutionThread;
import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;
import com.erakitin.wakie.testtask.presentation.App;
import com.erakitin.wakie.testtask.presentation.executor.AppExecutor;
import com.erakitin.wakie.testtask.presentation.executor.UiThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by erakitin on 22/01/16.
 */
@Module
public class AppModule {

    private App mApp;

    public AppModule(App app) {
        this.mApp = app;
    }

    @Provides
    public Application provideApplication() {
        return mApp;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(AppExecutor appExecutor) {
        return appExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UiThread uiThread) {
        return uiThread;
    }

}
