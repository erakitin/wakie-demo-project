package com.erakitin.wakie.testtask.data.datastore;

import com.erakitin.wakie.testtask.data.factories.ActiveTopicNotificationFactory;
import com.erakitin.wakie.testtask.data.factories.NewTopicNotificationFactory;
import com.erakitin.wakie.testtask.data.factories.TopicUpdateNotificationFactory;
import com.erakitin.wakie.testtask.data.model.ServerActiveTopicNotification;
import com.erakitin.wakie.testtask.data.model.ServerNewTopicNotification;
import com.erakitin.wakie.testtask.data.model.ServerTopicUpdateNotification;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class CloudNotificationDataStore implements INotificationDataStore {

    private static final long ACTIVE_TOPIC_EMISSION_INTERVAL = 10000;
    private static final long NEW_TOPIC_EMISSION_INTERVAL    = 15000;
    private static final long TOPIC_UPDATE_EMISSION_INTERVAL = 13000;

    private final ServerTopicListStorage mServerTopicListStorage;
    private final ActiveTopicNotificationFactory mActiveTopicNotificationFactory;
    private final NewTopicNotificationFactory mNewTopicNotificationFactory;
    private final TopicUpdateNotificationFactory mTopicUpdateNotificationFactory;

    @Inject
    public CloudNotificationDataStore(ServerTopicListStorage serverTopicListStorage,
                                      ActiveTopicNotificationFactory activeTopicNotificationFactory,
                                      NewTopicNotificationFactory newTopicNotificationFactory,
                                      TopicUpdateNotificationFactory topicUpdateNotificationFactory) {
        mServerTopicListStorage = serverTopicListStorage;
        mActiveTopicNotificationFactory = activeTopicNotificationFactory;
        mNewTopicNotificationFactory = newTopicNotificationFactory;
        mTopicUpdateNotificationFactory = topicUpdateNotificationFactory;
    }

    @Override
    public Observable<ServerActiveTopicNotification> getActiveTopicNotification() {
        return Observable
                .interval(ACTIVE_TOPIC_EMISSION_INTERVAL, ACTIVE_TOPIC_EMISSION_INTERVAL, TimeUnit.MILLISECONDS)
                .map(aLong -> mActiveTopicNotificationFactory.buildNotification(mServerTopicListStorage.getTopicList()));
    }

    @Override
    public Observable<ServerNewTopicNotification> getNewTopicNotification() {
        return Observable
                .interval(NEW_TOPIC_EMISSION_INTERVAL, NEW_TOPIC_EMISSION_INTERVAL, TimeUnit.MILLISECONDS)
                .map(aLong -> {
                    ServerNewTopicNotification notification = mNewTopicNotificationFactory.buildNotification();
                    mServerTopicListStorage.addTopic(notification.getTopic());
                    return notification;
                });
    }

    @Override
    public Observable<ServerTopicUpdateNotification> getTopicUpdateNotification() {
        return Observable.interval(TOPIC_UPDATE_EMISSION_INTERVAL, TOPIC_UPDATE_EMISSION_INTERVAL, TimeUnit.MILLISECONDS)
                .map(aLong -> mTopicUpdateNotificationFactory.buildNotificaton(mServerTopicListStorage.getTopicList()));
    }
}
