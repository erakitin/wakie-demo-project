package com.erakitin.wakie.testtask.data.dagger;

import com.erakitin.wakie.testtask.data.datastore.INotificationDataStore;
import com.erakitin.wakie.testtask.data.datastore.ITopicDataStore;
import com.erakitin.wakie.testtask.data.mapper.ActiveTopicNotificationDataMapper;
import com.erakitin.wakie.testtask.data.mapper.NewTopicNotificationDataMapper;
import com.erakitin.wakie.testtask.data.mapper.TopicDataMapper;
import com.erakitin.wakie.testtask.data.mapper.TopicUpdateNotificationDataMapper;
import com.erakitin.wakie.testtask.data.repository.NotificationRepository;
import com.erakitin.wakie.testtask.data.repository.TopicRepository;
import com.erakitin.wakie.testtask.domain.repository.INotificationRepository;
import com.erakitin.wakie.testtask.domain.repository.ITopicRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by erakitin on 22/01/16.
 */
@Module
public class RepositoryModule {

    @Singleton
    @Provides
    public INotificationRepository provideNotificationRepository(INotificationDataStore notificationDataStore,
                                                                 ActiveTopicNotificationDataMapper activeTopicNotificationDataMapper,
                                                                 NewTopicNotificationDataMapper newTopicNotificationDataMapper,
                                                                 TopicUpdateNotificationDataMapper topicUpdateNotificationDataMapper) {
        return new NotificationRepository(notificationDataStore,
                activeTopicNotificationDataMapper,
                newTopicNotificationDataMapper,
                topicUpdateNotificationDataMapper);
    }

    @Singleton
    @Provides
    public ITopicRepository provideTopicRepository(ITopicDataStore topicDataStore,
                                                   TopicDataMapper topicDataMapper) {
        return new TopicRepository(topicDataStore, topicDataMapper);
    }
}
