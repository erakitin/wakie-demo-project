package com.erakitin.wakie.testtask.domain.interactor;

import com.erakitin.wakie.testtask.domain.executor.PostExecutionThread;
import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;
import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.domain.repository.ITopicRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class GetTopicsUseCase extends UseCase<List<Topic>> {

    private final ITopicRepository mTopicRepository;

    private long mLastId;
    private int mLimit;

    @Inject
    public GetTopicsUseCase(ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread,
                            ITopicRepository topicRepository) {
        super(threadExecutor, postExecutionThread);
        mTopicRepository = topicRepository;
    }

    @Override
    protected Observable<List<Topic>> createUseCaseObservable() {
        return mTopicRepository.getTopics(mLastId, mLimit);
    }

    public void setLastId(long lastId) {
        mLastId = lastId;
    }

    public void setLimit(int limit) {
        mLimit = limit;
    }
}
