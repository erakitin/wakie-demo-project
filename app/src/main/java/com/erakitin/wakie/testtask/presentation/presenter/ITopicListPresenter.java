package com.erakitin.wakie.testtask.presentation.presenter;

import com.erakitin.wakie.testtask.presentation.view.ITopicListView;

/**
 * Created by erakitin on 24/01/16.
 */
public interface ITopicListPresenter {
    void init(ITopicListView view);

    void onDestroy();
}
