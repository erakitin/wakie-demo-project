package com.erakitin.wakie.testtask.data.factories;

import com.erakitin.wakie.testtask.data.model.ServerUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class UserFactory {

    @Inject
    public UserFactory() {
    }

    public List<ServerUser> buildUserList() {
        List<ServerUser> userList = new ArrayList<>();
        for (int i = 0; i < NAMES.length; i++) {
            ServerUser user = new ServerUser();
            user.setId(i + 1);
            user.setName(NAMES[i]);
            user.setAvatarUrl(AVATAR_URLS[i]);
            userList.add(user);
        }
        return userList;
    }

    private static final String[] NAMES = {
            "Nick Suwyn",
            "Alan Francis",
            "Dominik Suszczewicz",
            "Marcos Placona",
            "Rezaul Karim",
            "Ivan Smolyar",
            "Andrew Thompson",
            "Lawtonj94",
            "Gaurav Gupta",
            "Víctor Albertos",
            "Stephane Mathis",
            "Praveen Gopal",
            "Hong Ha Nguyen",
            "Dinesh Kannan",
            "Andriy Ivaneyko",
            "Adam Michalik",
            "Omar Hassan",
            "Alex Hales"
    };

    private static final String[] AVATAR_URLS = {
            "https://i.stack.imgur.com/NJcqr.png?s=328&g=1",
            "https://www.gravatar.com/avatar/0f4cefeedec5163556751d61625eedd0?s=328&d=identicon&r=PG",
            "https://www.gravatar.com/avatar/01d6005ebf89c14de87a62add488aaad?s=328&d=identicon&r=PG",
            "https://i.stack.imgur.com/GLKUy.jpg?s=328&g=1",
            "https://i.stack.imgur.com/tj0qU.jpg?s=328&g=1",
            "https://www.gravatar.com/avatar/495d675e3bc42ed1dee469e2ce701f1b?s=328&d=identicon&r=PG",
            "https://i.stack.imgur.com/utY5u.jpg?s=328&g=1",
            "https://www.gravatar.com/avatar/00aa1356e6f90fca08b36fb3c8d230c5?s=328&d=identicon&r=PG",
            "https://i.stack.imgur.com/aUihh.jpg?s=328&g=1",
            "https://www.gravatar.com/avatar/1e5af4a99e16f69535435d4c7a568dd9?s=328&d=identicon&r=PG",
            "https://www.gravatar.com/avatar/0ff652685122f4500dd4f3c21fb32da2?s=328&d=identicon&r=PG",
            "https://www.gravatar.com/avatar/33526616d4a11f1630d4939b769cfbf9?s=328&d=identicon&r=PG",
            "https://www.gravatar.com/avatar/12679d1247d3b8a4cea73655d1047786?s=328&d=identicon&r=PG",
            "https://i.stack.imgur.com/VcN72.jpg?s=328&g=1",
            null,
            null,
            null,
            null
    };
}
