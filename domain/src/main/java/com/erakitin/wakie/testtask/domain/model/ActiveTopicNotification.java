package com.erakitin.wakie.testtask.domain.model;

/**
 * Created by erakitin on 21/01/16.
 */
public class ActiveTopicNotification extends Entity {

    private final long mTopicId;

    public long getTopicId() {
        return mTopicId;
    }

    private ActiveTopicNotification(Builder builder) {
        super(builder.mId);
        mTopicId = builder.mTopicId;
    }

    public static class Builder implements EntityBuilder<ActiveTopicNotification> {
        private long mId;
        private long mTopicId;

        public Builder id(long id) {
            mId = id;
            return this;
        }

        public Builder topicId(long topicId) {
            this.mTopicId = topicId;
            return this;
        }

        @Override
        public ActiveTopicNotification build() {
            return new ActiveTopicNotification(this);
        }
    }
}
