package com.erakitin.wakie.testtask.data.factories;

import com.erakitin.wakie.testtask.data.model.ServerNewTopicNotification;
import com.erakitin.wakie.testtask.data.model.ServerTopic;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class NewTopicNotificationFactory {

    private long mLastId = 0;

    private final TopicFactory mTopicFactory;

    @Inject
    public NewTopicNotificationFactory(TopicFactory topicFactory) {
        mTopicFactory = topicFactory;
    }

    public ServerNewTopicNotification buildNotification() {
        ServerTopic topic = mTopicFactory.buildTopic(System.currentTimeMillis());

        ServerNewTopicNotification notification = new ServerNewTopicNotification();
        notification.setId(++mLastId);
        notification.setTopic(topic);

        return notification;
    }
}
