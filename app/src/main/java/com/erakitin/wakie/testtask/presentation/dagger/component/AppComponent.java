package com.erakitin.wakie.testtask.presentation.dagger.component;

import com.erakitin.wakie.testtask.data.dagger.DataStoreModule;
import com.erakitin.wakie.testtask.data.dagger.RepositoryModule;
import com.erakitin.wakie.testtask.domain.executor.PostExecutionThread;
import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;
import com.erakitin.wakie.testtask.domain.repository.INotificationRepository;
import com.erakitin.wakie.testtask.domain.repository.ITopicRepository;
import com.erakitin.wakie.testtask.presentation.App;
import com.erakitin.wakie.testtask.presentation.dagger.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by erakitin on 22/01/16.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                DataStoreModule.class,
                RepositoryModule.class
        })
public interface AppComponent {

    void inject(App app);

    ThreadExecutor getThreadExecutor();

    PostExecutionThread getPostExecutionThread();

    INotificationRepository getNotificationRepository();

    ITopicRepository getTopicRepository();
}
