package com.erakitin.wakie.testtask.domain.model;

import java.util.Date;

/**
 * Created by erakitin on 21/01/16.
 */
public class Topic extends Entity {

    private final User mUser;
    private final String mText;
    private final Date mCreationTime;
    private final int mCommentsCount;

    private Topic(Builder builder) {
        super(builder.mId);
        mUser = builder.mUser;
        mText = builder.mText;
        mCreationTime = builder.mCreationTime;
        mCommentsCount = builder.mCommentsCount;
    }

    public User getUser() {
        return mUser;
    }

    public String getText() {
        return mText;
    }

    public Date getCreationTime() {
        return mCreationTime;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public static class Builder implements EntityBuilder<Topic> {

        private long mId;
        private User mUser;
        private String mText;
        private Date mCreationTime;
        private int mCommentsCount;

        public Builder id(long id) {
            mId = id;
            return this;
        }

        public Builder user(User user) {
            mUser = user;
            return this;
        }

        public Builder text(String text) {
            mText = text;
            return this;
        }

        public Builder creationTime(Date creationTime) {
            mCreationTime = creationTime;
            return this;
        }

        public Builder commentsCount(int commentsCount) {
            mCommentsCount = commentsCount;
            return this;
        }

        @Override
        public Topic build() {
            return new Topic(this);
        }
    }
}
