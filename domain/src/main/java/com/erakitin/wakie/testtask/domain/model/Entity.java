package com.erakitin.wakie.testtask.domain.model;

/**
 * Created by erakitin on 21/01/16.
 */
public abstract class Entity {

    private final long mId;

    public long getId() {
        return mId;
    }

    public Entity(long id) {
        mId = id;
    }
}
