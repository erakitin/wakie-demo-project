package com.erakitin.wakie.testtask.data.model;

/**
 * Created by erakitin on 21/01/16.
 */
public class ServerActiveTopicNotification extends ServerEntity {

    private long mTopicId;

    public long getTopicId() {
        return mTopicId;
    }

    public void setTopicId(long topicId) {
        mTopicId = topicId;
    }
}
