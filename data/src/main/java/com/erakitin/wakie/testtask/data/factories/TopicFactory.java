package com.erakitin.wakie.testtask.data.factories;

import com.erakitin.wakie.testtask.data.model.ServerTopic;
import com.erakitin.wakie.testtask.data.model.ServerUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by erakitin on 21/01/16.
 */
@Singleton
public class TopicFactory {

    private static final long INITIAL_MAX_COMMENT_ID = 54L;

    private static final int MAX_COMMENT_COUNT           = 100;
    private static final int MAX_INTERVAL_BETWEEN_TOPICS = 2 * 60 * 60 * 1000;

    private final Random mRandom = new Random();
    private final List<ServerUser> mUserList;

    private long mOldestTopicCreationTime = System.currentTimeMillis();
    private long mMaxTopicId = INITIAL_MAX_COMMENT_ID;

    @Inject
    public TopicFactory(UserFactory userFactory) {
        mUserList = userFactory.buildUserList();
    }

    public ServerTopic buildTopic(long createdAt) {
        return buildTopic(mMaxTopicId++, createdAt);
    }

    public ServerTopic buildTopic(long id, long createdAt) {
        ServerTopic topic = new ServerTopic();
        topic.setId(id);
        topic.setText(getRandomQuestion());
        topic.setCommentsCount(getRandomCommentsCount());
        topic.setTimestamp(createdAt);
        topic.setUser(getRandomUser());
        return topic;
    }

    public List<ServerTopic> buildTopicList(int limit) {
        return buildTopicList(INITIAL_MAX_COMMENT_ID, limit);
    }

    public List<ServerTopic> buildTopicList(long lastId, int limit) {
        List<ServerTopic> topicList = new ArrayList<>();
        for (int i = 0; i < limit && lastId > 1; i++) {
            mOldestTopicCreationTime -= mRandom.nextInt(MAX_INTERVAL_BETWEEN_TOPICS);
            lastId--;
            ServerTopic topic = buildTopic(lastId, mOldestTopicCreationTime);
            topicList.add(topic);
        }
        return topicList;
    }

    public void modifyTopic(ServerTopic topic) {
        int newCommentsCount = topic.getCommentsCount() + getRandomCommentsCount();
        topic.setCommentsCount(newCommentsCount);
        topic.setText(getRandomQuestion());
    }

    private String getRandomQuestion() {
        return QUESTIONS[mRandom.nextInt(QUESTIONS.length)];
    }

    private int getRandomCommentsCount() {
        return mRandom.nextInt(MAX_COMMENT_COUNT);
    }

    private ServerUser getRandomUser() {
        return mUserList.get(mRandom.nextInt(mUserList.size()));
    }

    private static final String[] QUESTIONS = {
            "How to find steps with Google Fit API for Android?",
            "@Component spring annotation in wicket project?",
            "Trouble injecting a value into inner class with Spring?",
            "How to change an Image in JFrame when a timer reaches a number?",
            "How does a thread move from running to runnable state?",
            "How to maintain immersive mode when navigating to a new activity?",
            "What is different with two version code of my second activity?",
            "Where to change the Build Action for AndroidManifest.xml to “none?”",
            "Add the individual columns and rows in a 2D Array and display in a 1D Array table?",
            "ShadowGeocoder missing in Robolectric 3.0?",
            "How to store images from gallery or camera to my app?",
            "How to inject SessionFactory when persistence unit is defined?",
            "Interpolating UITextFields with UITextView using Text Kit?",
            "Can wprintf output be properly redirected to UTF-16 on Windows?",
            "Disable google maps tile fading or detect if map is fully rendered?",
            "How do I configure my uWsgi server to protect against the Unreadable Post Error?",
            "How can I serve an AngularJS 2 app without having to also serve all the files in `node_modules`?",
            "How to use HTML5shiv correctly… how work on IE 9, Firefox, Safari?",
            "How can I implement HATEOAS in Haskell?",
            "Steam API login isn't working - Is there another way?",
            "How to deal with Code Contracts warning CC1036 when using string.IsNullOrWhiteSpace?",
            "What is a good name for a concurrency daemon that spawns new workers?",
            "Why is writing to a buffer from within a fragment shader disallowed in Metal?",
            "How to fetch all gallery image of mobile using cordova?",
            "Why does AngularJS show me that a scope variable is undefined in directive when it is clearly defined?"
    };
}
