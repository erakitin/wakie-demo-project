package com.erakitin.wakie.testtask.data.repository;

import com.erakitin.wakie.testtask.data.datastore.ITopicDataStore;
import com.erakitin.wakie.testtask.data.mapper.TopicDataMapper;
import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.domain.repository.ITopicRepository;

import java.util.List;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class TopicRepository implements ITopicRepository {

    private final ITopicDataStore mTopicDataStore;
    private final TopicDataMapper mTopicDataMapper;

    public TopicRepository(ITopicDataStore topicDataStore,
                           TopicDataMapper topicDataMapper) {
        mTopicDataStore = topicDataStore;
        mTopicDataMapper = topicDataMapper;
    }

    @Override
    public Observable<List<Topic>> getTopics(long lastId, int limit) {
        return mTopicDataStore.getTopics(lastId, limit)
                .compose(mTopicDataMapper.mapList());
    }

}
