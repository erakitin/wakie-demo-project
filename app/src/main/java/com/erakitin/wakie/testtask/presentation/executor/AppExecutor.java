package com.erakitin.wakie.testtask.presentation.executor;

import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;

/**
 * Created by erakitin on 22/01/16.
 */
public class AppExecutor implements ThreadExecutor {
    private static int NUM_THREADS = 8;

    private final Executor mExecutor = Executors.newFixedThreadPool(NUM_THREADS);

    @Inject
    public AppExecutor() {
    }

    @Override
    public void execute(Runnable command) {
        if (command == null) {
            throw new IllegalArgumentException("Runnable to execute cannot be null");
        }
        mExecutor.execute(command);
    }
}
