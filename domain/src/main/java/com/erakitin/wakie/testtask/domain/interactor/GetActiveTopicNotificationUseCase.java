package com.erakitin.wakie.testtask.domain.interactor;

import com.erakitin.wakie.testtask.domain.executor.PostExecutionThread;
import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;
import com.erakitin.wakie.testtask.domain.model.ActiveTopicNotification;
import com.erakitin.wakie.testtask.domain.repository.INotificationRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class GetActiveTopicNotificationUseCase extends UseCase<ActiveTopicNotification> {

    private final INotificationRepository mNotificationRepository;

    @Inject
    public GetActiveTopicNotificationUseCase(ThreadExecutor threadExecutor,
                                             PostExecutionThread postExecutionThread,
                                             INotificationRepository notificationRepository) {
        super(threadExecutor, postExecutionThread);
        mNotificationRepository = notificationRepository;
    }

    @Override
    protected Observable<ActiveTopicNotification> createUseCaseObservable() {
        return mNotificationRepository.getActiveTopicNotification();
    }
}
