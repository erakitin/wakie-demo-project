package com.erakitin.wakie.testtask.domain.interactor;

import android.util.Log;

import com.erakitin.wakie.testtask.domain.executor.PostExecutionThread;
import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by erakitin on 22/01/16.
 */
public abstract class UseCase<T> {
    private static final String TAG = "UseCase";

    protected final ThreadExecutor mThreadExecutor;
    protected final PostExecutionThread mPostExecutionThread;

    protected Subscription mSubscription = Subscriptions.unsubscribed();

    protected UseCase(ThreadExecutor threadExecutor,
                      PostExecutionThread postExecutionThread) {
        mThreadExecutor = threadExecutor;
        mPostExecutionThread = postExecutionThread;
    }

    public void execute(Subscriber<T> useCaseSubscriber) {
        Log.d(TAG, "executing usecase: " + getClass().getSimpleName());

        if (!mSubscription.isUnsubscribed()) {
            Log.w(TAG, String.format("usecase: %s, execute, previous subscription could be leaked", getClass().getSimpleName()));
        }

        mSubscription = createUseCaseObservable()
                .subscribeOn(Schedulers.from(mThreadExecutor))
                .observeOn(mPostExecutionThread.getScheduler())
                .subscribe(useCaseSubscriber);
    }

    public void unsubscribe() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    protected abstract Observable<T> createUseCaseObservable();
}
