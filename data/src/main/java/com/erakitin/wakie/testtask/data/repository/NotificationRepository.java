package com.erakitin.wakie.testtask.data.repository;

import com.erakitin.wakie.testtask.data.datastore.INotificationDataStore;
import com.erakitin.wakie.testtask.data.mapper.ActiveTopicNotificationDataMapper;
import com.erakitin.wakie.testtask.data.mapper.NewTopicNotificationDataMapper;
import com.erakitin.wakie.testtask.data.mapper.TopicUpdateNotificationDataMapper;
import com.erakitin.wakie.testtask.domain.model.ActiveTopicNotification;
import com.erakitin.wakie.testtask.domain.model.NewTopicNotification;
import com.erakitin.wakie.testtask.domain.model.TopicUpdateNotification;
import com.erakitin.wakie.testtask.domain.repository.INotificationRepository;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class NotificationRepository implements INotificationRepository {

    private final INotificationDataStore mNotificationDataStore;
    private final ActiveTopicNotificationDataMapper mActiveTopicNotificationDataMapper;
    private final NewTopicNotificationDataMapper mNewTopicNotificationDataMapper;
    private final TopicUpdateNotificationDataMapper mTopicUpdateNotificationDataMapper;

    public NotificationRepository(INotificationDataStore notificationDataStore,
                                  ActiveTopicNotificationDataMapper activeTopicNotificationDataMapper,
                                  NewTopicNotificationDataMapper newTopicNotificationDataMapper,
                                  TopicUpdateNotificationDataMapper topicUpdateNotificationDataMapper) {
        mNotificationDataStore = notificationDataStore;
        mActiveTopicNotificationDataMapper = activeTopicNotificationDataMapper;
        mNewTopicNotificationDataMapper = newTopicNotificationDataMapper;
        mTopicUpdateNotificationDataMapper = topicUpdateNotificationDataMapper;
    }

    @Override
    public Observable<ActiveTopicNotification> getActiveTopicNotification() {
        return mNotificationDataStore.getActiveTopicNotification()
                .compose(mActiveTopicNotificationDataMapper.map());
    }

    @Override
    public Observable<NewTopicNotification> getNewTopicNotification() {
        return mNotificationDataStore.getNewTopicNotification()
                .compose(mNewTopicNotificationDataMapper.map());
    }

    @Override
    public Observable<TopicUpdateNotification> getTopicUpdateNotification() {
        return mNotificationDataStore.getTopicUpdateNotification()
                .compose(mTopicUpdateNotificationDataMapper.map());
    }
}
