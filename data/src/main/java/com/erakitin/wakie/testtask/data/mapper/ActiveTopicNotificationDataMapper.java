package com.erakitin.wakie.testtask.data.mapper;

import com.erakitin.wakie.testtask.data.model.ServerActiveTopicNotification;
import com.erakitin.wakie.testtask.domain.model.ActiveTopicNotification;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class ActiveTopicNotificationDataMapper extends BaseDataMapper<ServerActiveTopicNotification, ActiveTopicNotification> {

    @Inject
    public ActiveTopicNotificationDataMapper() {
    }

    @Override
    protected ActiveTopicNotification transform(ServerActiveTopicNotification serverEntity) {
        return new ActiveTopicNotification.Builder()
                .id(serverEntity.getId())
                .topicId(serverEntity.getTopicId())
                .build();
    }
}
