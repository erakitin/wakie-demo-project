package com.erakitin.wakie.testtask.data.mapper;

import com.erakitin.wakie.testtask.data.model.ServerNewTopicNotification;
import com.erakitin.wakie.testtask.domain.model.NewTopicNotification;
import com.erakitin.wakie.testtask.domain.model.Topic;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class NewTopicNotificationDataMapper extends BaseDataMapper<ServerNewTopicNotification, NewTopicNotification> {

    private final TopicDataMapper mTopicDataMapper;

    @Inject
    public NewTopicNotificationDataMapper(TopicDataMapper topicDataMapper) {
        mTopicDataMapper = topicDataMapper;
    }

    @Override
    protected NewTopicNotification transform(ServerNewTopicNotification serverEntity) {
        Topic topic = mTopicDataMapper.transform(serverEntity.getTopic());
        return new NewTopicNotification.Builder()
                .id(serverEntity.getId())
                .topic(topic)
                .build();
    }
}
