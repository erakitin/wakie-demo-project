package com.erakitin.wakie.testtask.data.mapper;

import com.erakitin.wakie.testtask.data.model.ServerEntity;
import com.erakitin.wakie.testtask.domain.model.Entity;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by erakitin on 20/01/16.
 */
public abstract class BaseDataMapper<T extends ServerEntity, R extends Entity> {

    private static final String TAG = BaseDataMapper.class.getSimpleName();

    private Function<T, R> mTransformFunction = this::transform;

    public Observable.Transformer<T, R> map() {
        return observable -> observable
                .map(this::transform);
    }

    public Observable.Transformer<List<T>, List<R>> mapList() {
        return observable -> observable.map(this::transformList);
    }

    public List<R> transformList(List<T> serverModelList) {
        return new ArrayList<>(Collections2.transform(serverModelList, mTransformFunction));
    }

    protected abstract R transform(T serverEntity);

}
