package com.erakitin.wakie.testtask.domain.interactor;

import com.erakitin.wakie.testtask.domain.executor.PostExecutionThread;
import com.erakitin.wakie.testtask.domain.executor.ThreadExecutor;
import com.erakitin.wakie.testtask.domain.model.NewTopicNotification;
import com.erakitin.wakie.testtask.domain.repository.INotificationRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by erakitin on 22/01/16.
 */
public class GetNewTopicNotificationUseCase extends UseCase<NewTopicNotification> {

    private final INotificationRepository mNotificationRepository;

    @Inject
    public GetNewTopicNotificationUseCase(ThreadExecutor threadExecutor,
                                          PostExecutionThread postExecutionThread,
                                          INotificationRepository notificationRepository) {
        super(threadExecutor, postExecutionThread);
        mNotificationRepository = notificationRepository;
    }

    @Override
    protected Observable<NewTopicNotification> createUseCaseObservable() {
        return mNotificationRepository.getNewTopicNotification();
    }
}
