package com.erakitin.wakie.testtask.domain.model;

/**
 * Created by erakitin on 21/01/16.
 */
public class TopicUpdateNotification extends Entity {

    private Topic mTopic;

    public Topic getTopic() {
        return mTopic;
    }

    private TopicUpdateNotification(Builder builder) {
        super(builder.mId);
        mTopic = builder.mTopic;
    }

    public static class Builder implements EntityBuilder<TopicUpdateNotification> {

        private long mId;
        private Topic mTopic;

        public Builder id(long id) {
            mId = id;
            return this;
        }

        public Builder topic(Topic topic) {
            mTopic = topic;
            return this;
        }

        @Override
        public TopicUpdateNotification build() {
            return new TopicUpdateNotification(this);
        }
    }
}
