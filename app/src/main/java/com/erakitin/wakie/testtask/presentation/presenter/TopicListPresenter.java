package com.erakitin.wakie.testtask.presentation.presenter;

import com.erakitin.wakie.testtask.domain.interactor.DefaultSubscriber;
import com.erakitin.wakie.testtask.domain.interactor.GetActiveTopicNotificationUseCase;
import com.erakitin.wakie.testtask.domain.interactor.GetNewTopicNotificationUseCase;
import com.erakitin.wakie.testtask.domain.interactor.GetTopicUpdateNotificationUseCase;
import com.erakitin.wakie.testtask.domain.interactor.GetTopicsUseCase;
import com.erakitin.wakie.testtask.domain.model.ActiveTopicNotification;
import com.erakitin.wakie.testtask.domain.model.NewTopicNotification;
import com.erakitin.wakie.testtask.domain.model.Topic;
import com.erakitin.wakie.testtask.domain.model.TopicUpdateNotification;
import com.erakitin.wakie.testtask.presentation.view.ITopicListView;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by erakitin on 24/01/16.
 */
public class TopicListPresenter implements ITopicListPresenter {

    private static final int TOPICS_LIMIT = 10;

    private ITopicListView mView;

    private final GetTopicsUseCase mGetTopicsUseCase;
    private final GetNewTopicNotificationUseCase mGetNewTopicNotificationUseCase;
    private final GetActiveTopicNotificationUseCase mGetActiveTopicNotificationUseCase;
    private final GetTopicUpdateNotificationUseCase mGetTopicUpdateNotificationUseCase;

    private List<Topic> mTopics;
    private Subscription mScrollSubscription;

    @Inject
    public TopicListPresenter(GetTopicsUseCase getTopicsUseCase,
                              GetNewTopicNotificationUseCase getNewTopicNotificationUseCase,
                              GetActiveTopicNotificationUseCase getActiveTopicNotificationUseCase,
                              GetTopicUpdateNotificationUseCase getTopicUpdateNotificationUseCase) {
        mGetTopicsUseCase = getTopicsUseCase;
        mGetNewTopicNotificationUseCase = getNewTopicNotificationUseCase;
        mGetActiveTopicNotificationUseCase = getActiveTopicNotificationUseCase;
        mGetTopicUpdateNotificationUseCase = getTopicUpdateNotificationUseCase;
    }

    @Override
    public void init(ITopicListView view) {
        mView = view;
        mView.showLoader();
        loadTopics(-1);
    }

    @Override
    public void onDestroy() {
        mGetTopicsUseCase.unsubscribe();
        mGetNewTopicNotificationUseCase.unsubscribe();
        mGetActiveTopicNotificationUseCase.unsubscribe();
        mGetTopicUpdateNotificationUseCase.unsubscribe();
    }

    private void loadTopics(long lastTopicId) {
        mGetTopicsUseCase.setLimit(TOPICS_LIMIT);
        mGetTopicsUseCase.setLastId(lastTopicId);
        mGetTopicsUseCase.execute(new DefaultSubscriber<List<Topic>>() {
            @Override
            public void onNext(List<Topic> topics) {
                super.onNext(topics);
                boolean hasMore = topics.size() == TOPICS_LIMIT;
                if (lastTopicId > 0) {
                    int start = mTopics.size();
                    int count = topics.size();
                    mTopics.addAll(topics);
                    mView.notifyTopicsInserted(start, count, hasMore);
                } else {
                    mTopics = topics;
                    mView.setTopics(topics, hasMore);
                    mView.hideLoader();
                    subscribeToNotifications();
                    if (hasMore) {
                        subscribeToScrollUpdates();
                    }
                }
                if (!hasMore) {
                    unsubscribeFromScrollUpdates();
                }
            }
        });
    }

    private void subscribeToScrollUpdates() {
        unsubscribeFromScrollUpdates();
        mScrollSubscription = mView.getScrollObservable()
                .map(integer -> getLastTopicId())
                .distinctUntilChanged()
                .doOnNext(this::loadTopics)
                .subscribe();
    }

    private void unsubscribeFromScrollUpdates() {
        if (mScrollSubscription != null) {
            mScrollSubscription.unsubscribe();
        }
    }

    private void subscribeToNotifications() {
        subscribeToTopicUpdates();
        subscribeToNewTopics();
        subscribeToActiveTopics();
        unsubscribeFromScrollUpdates();
    }

    private void subscribeToTopicUpdates() {
        mGetTopicUpdateNotificationUseCase.execute(new DefaultSubscriber<TopicUpdateNotification>() {
            @Override
            public void onNext(TopicUpdateNotification topicUpdateNotification) {
                super.onNext(topicUpdateNotification);
                Topic updatedTopic = topicUpdateNotification.getTopic();
                int position = getTopicPositionById(updatedTopic.getId());
                if (position < 0) {
                    return;
                }
                mTopics.remove(position);
                mTopics.add(position, updatedTopic);
                mView.notifyTopicChanged(position);
            }
        });
    }

    private void subscribeToNewTopics() {
        mGetNewTopicNotificationUseCase.execute(new DefaultSubscriber<NewTopicNotification>() {
            @Override
            public void onNext(NewTopicNotification newTopicNotification) {
                super.onNext(newTopicNotification);
                mTopics.add(0, newTopicNotification.getTopic());
                mView.notifyTopicInsertedToTop();
            }
        });
    }

    private void subscribeToActiveTopics() {
        mGetActiveTopicNotificationUseCase.execute(new DefaultSubscriber<ActiveTopicNotification>() {
            @Override
            public void onNext(ActiveTopicNotification activeTopicNotification) {
                super.onNext(activeTopicNotification);
                mView.setActiveTopicId(activeTopicNotification.getTopicId());
            }
        });
    }

    private int getTopicPositionById(long id) {
        for (int i = 0; i < mTopics.size(); i++) {
            if (mTopics.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    private long getLastTopicId() {
        return mTopics.get(mTopics.size() - 1).getId();
    }

}
