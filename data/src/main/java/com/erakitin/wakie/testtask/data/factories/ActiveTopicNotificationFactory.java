package com.erakitin.wakie.testtask.data.factories;

import com.erakitin.wakie.testtask.data.model.ServerActiveTopicNotification;
import com.erakitin.wakie.testtask.data.model.ServerTopic;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

/**
 * Created by erakitin on 21/01/16.
 */
public class ActiveTopicNotificationFactory {

    private long mLastId = 0;

    private final Random mRandom = new Random();

    @Inject
    public ActiveTopicNotificationFactory() {
    }

    public ServerActiveTopicNotification buildNotification(List<ServerTopic> topicList) {
        ServerTopic randomTopic = topicList.get(mRandom.nextInt(topicList.size()));

        ServerActiveTopicNotification notification = new ServerActiveTopicNotification();
        notification.setId(++mLastId);
        notification.setTopicId(randomTopic.getId());
        return notification;
    }
}
