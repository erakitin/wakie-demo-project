package com.erakitin.wakie.testtask.domain.executor;

import java.util.concurrent.Executor;

/**
 * Created by erakitin on 22/01/16.
 */
public interface ThreadExecutor extends Executor {
}